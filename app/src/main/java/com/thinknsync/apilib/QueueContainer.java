package com.thinknsync.apilib;

import com.android.volley.Request;

public interface QueueContainer {
    <T> void addToRequestQueue(Request<T> req);
    void cancelPendingRequests(Object tag);
}
