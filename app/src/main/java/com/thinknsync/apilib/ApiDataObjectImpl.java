package com.thinknsync.apilib;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 10/16/16.
 */

public class ApiDataObjectImpl implements ApiDataObject {

    private Object obj = null;
    private Map header = null;
    private String body = null;

    public ApiDataObjectImpl(Object object) {
        this.obj = object;
        validateObject(object);
    }

    @Override
    public Object getObject() {
        return obj;
    }

    @Override
    public Map getHeader() {
        return header;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public void setHeader(Map header) {
        this.header = header;
    }

    @Override
    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public void setObj(Object obj) {
        this.obj = obj;
    }

    @Override
    public String getObjectType() {
        if (obj instanceof Map) {
            return mapType;
        } else if (obj instanceof JSONObject) {
            return jsonType;
        } else if (obj == null || obj.equals(JSONObject.NULL)) {
            return nullType;
        }
        return undefinedType;
    }

    @Override
    public ApiDataObject addKeyValuePair(String key, String value) {
        addKeyValue(key, value);
        return this;
    }

    @Override
    public ApiDataObject addKeyValuePair(String key, int value) {
        addKeyValue(key, value);
        return this;
    }

    @Override
    public ApiDataObject addKeyValuePair(String key, boolean value) {
        addKeyValue(key, value);
        return this;
    }

    private void addKeyValue(String key, Object value){
        String objectType = getObjectType();

        if (objectType.equals(jsonType)) {
            try {
                ((JSONObject) obj).put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (objectType.equals(mapType)) {
            ((Map) obj).put(key, value);
        }
    }

    @Override
    public void removeKeyValuePair(String key) {
        String objectType = getObjectType();

        if (objectType.equals(jsonType)) {
            ((JSONObject) obj).remove(key);
        } else if (objectType.equals(mapType)) {
            ((Map) obj).remove(key);
        }
    }

    @Override
    public void validateObject(Object object) {
        try {
            if(isNull()){
                throw new NullPointerException();
            } else if (!isDefinedObject()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    private boolean isDefinedObject(){
        if(getObjectType().equals(undefinedType)){
            return false;
        }
        return true;
    }

    private boolean isNull(){
        if(getObjectType().equals(nullType)){
            return true;
        }
        return false;
    }
}
