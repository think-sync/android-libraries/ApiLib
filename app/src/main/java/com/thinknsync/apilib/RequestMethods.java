package com.thinknsync.apilib;

import com.android.volley.Request;

public interface RequestMethods {
    int METHOD_POST = Request.Method.POST;
    int METHOD_GET = Request.Method.GET;
}
